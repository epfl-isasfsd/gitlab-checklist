#!/usr/bin/env node

const puppeteer = require('puppeteer')
const url = require('url')
const rp = require('request-promise-native');

var argv = require('yargs')
    .usage('Usage: ./$0 (-u [string])') // ./index.js -p444
    .default('u', 'gitlab.epfl.ch')
    .argv;

(async () => {
  const browser = await puppeteer.launch({headless: false})
  let page = await browser.newPage()
  await page.setDefaultNavigationTimeout(0)
  await page.goto(urlInSite())

  await checkTequilaLogin(browser, page)
  await userLogsInThroughTequila(browser, page)
  const username = await getLoggedUsername(page)
  console.log(' → OK:', username, 'logged in')

  await checkProjectAvatars(browser, page)
  await checkUserAvatar(page)
  await checkList(page)
  console.log(' - ...')
  await page.screenshot({path: "gitlab-screenshot.png"})
  console.log(' - ...')
  await browser.close();
  console.log(' - ...')
})();

////////////////////////////////////////////////////////////////////////////////

function gitlabURL(testURL) {
  if (!/^((http[s]?:)?\/\/)/.test(testURL)) {
    testURL = "https://" + testURL
  }
  const urlObject = url.parse(testURL)
  // console.log("Testing:", url.format(urlObject))
  return url.format(urlObject)
}


async function checkTequilaLogin(browser, page) {
  try {
    await page.waitForSelector("#oauth-login-tequila", {visible: true, timeout: 300})
    console.log(" → Tequila login bouton found")
    console.log(" → Waiting for login")
  } catch(e) {
    console.log(" ✗ Tequila login bouton not found")
    try {
      await browser.close()
      process.exit()
    } catch(e) {}
  }
}

async function userLogsInThroughTequila(browser, page) {
  await page.waitForSelector(".avatar-container", {visible: true, timeout: 0})
}

async function getLoggedUsername(page) {
  const username = await page.$eval('li.header-user .header-user-dropdown-toggle', a=>a.href)
  return url.parse(username).path.substring(1)
}

// Looking for at least a DIV (project default avatar) and an image tag
// (project user-defined image)
async function checkProjectAvatars(browser, page) {
  const subEl = await page.evaluate(() => {
    const elements = document.querySelectorAll('.avatar-container > a.project > *');
    return Array.from(elements).map(element => element.tagName);
  });

  if (subEl.includes('DIV') && subEl.includes('IMG')) {
    console.log(" → OK: Projects' avatars")
    return true
  } else {
    console.log(" ✗ Projects' avatar not OK")
    return false
  }
}

async function checkUserAvatar(page) {
  //let gravatar = await page.$('.qa-user-avatar > img')
  let result = await page.waitForFunction(function() {
    const src = $(".qa-user-avatar").attr("src");
    //console.log(src);
    return (src && ! src.startsWith("data:")) ? src : null;
  })
  result = await result.jsonValue()
  
  // bob@yopmail.com / toto123...

  console.log(result)
  console.log(urlInSite(result))
  debugger
  await rp({url: urlInSite(result),
            followRedirect: false});
    
}

async function checkList(page) {
  let avatars = await page.$('.project-avatar')
  if (avatars.length < 1) {
    throw new Error('no avatars found')
  }

  let gravatarUrl = await page.$eval('.qa-user-avatar', el => $(el).attr("src"))
  console.log(gravatarUrl)
}

function urlInSite(opt_suffix) {
  prefix = gitlabURL(argv.u)
  if (opt_suffix) {
    // TODO: it depends on what is at the beginning of opt_suffix
    return prefix + opt_suffix
  } else {
    return prefix
  }
}